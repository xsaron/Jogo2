package spaceInvaders;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Application extends JFrame {

	public Application() {
    	
        add(new Map());

        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Space Combat Game");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
    
    public static void main(String[] args) {
    	Menu menu = new Application() .new Menu();
    	menu.setVisible(true);
    	
       
    }
    
    public class Menu extends JFrame {
    	
    	public Menu() {
    		
    		setSize(Game.getWidth(), Game.getHeight());
    		setTitle("Space Through Game");
    		setResizable(false);
    		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLocationRelativeTo(null);
            
            JButton startButton = new JButton("Jogar!");
            JButton rankingButton = new JButton("Ranking");
            
            Dimension buttonDimension = new Dimension(120, 60);
            Dimension panelDimension = new Dimension(Game.getWidth(), 60);
            
            startButton.setPreferredSize(buttonDimension);
            rankingButton.setPreferredSize(buttonDimension);
            Event f = new Event();
            startButton.addActionListener(f);
            rankingButton.addActionListener(new ActionListener() {
            	public void actionPerformed(ActionEvent evt) {
            		RankingMenu ranking = new RankingMenu();
            		ranking.setVisible(true);
            	}
            });
            
            JPanel buttonPanel = new JPanel();
         
            buttonPanel.add(startButton);
            buttonPanel.add(rankingButton);
            buttonPanel.setPreferredSize(panelDimension);
            
            add(buttonPanel);
           
    	}
    	
    }
    
    public class Event implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent f) {
			EventQueue.invokeLater(new Runnable() {
 	            @Override
 	            public void run() {
 	                setVisible(false);

 	            	Application app = new Application();
 	                app.setVisible(true);
 	            }
 	        });	
		}
    }
}


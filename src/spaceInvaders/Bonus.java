package spaceInvaders;

public class Bonus extends Sprite{
	private static final int BONUS_SPEED = 1;
	
	public Bonus(Position position) {
		super(position);
	}
	
	public void move() {
		Position position = this.position;
		loadImage("images/bonus.gif");
//		System.out.println("entra move bonus");
		position.setY(position.getY() + BONUS_SPEED);
	}
}

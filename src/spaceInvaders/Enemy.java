package spaceInvaders;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Enemy extends Sprite{
	
	public final int EASY = 1;
	public final int MEDIUM = 2;
	public final int HARD = 3;
//	private int enemyCondition = 0;
	
	public Enemy(){
		super(new Position(0,0));
	}

	public Enemy(Position position) {
		super(position);
//		setEnemyCondition(getEnemyCondition() + 1);/
	}
	
	public void fly(int dificult) {
		switch(dificult) {
			case EASY:
				flyEasy();
				break;
			case MEDIUM:
				flyMedium();
				break;
			case HARD:
				flyHard();
				break;
		}
	}
	
	private void flyEasy() {
		Position position = this.position;
		loadImage("images/alien_EASY.gif");
		position.setY(position.getY() + EASY);
		
	}
	
	private void flyMedium() {
		Position position = this.position;
		loadImage("images/alien_MEDIUM.gif");
		position.setY(position.getY() + MEDIUM);
	}
	
	private void flyHard() {
		Position position = this.position;
		loadImage("images/alien_HARD.gif");
		position.setY(position.getY() + HARD);
	}
	
}

package spaceInvaders;

import java.util.ArrayList;

public class Explosion extends Sprite{
	
	public Explosion(Position position) {
		super(position);
		loadExplosionGif();
	}
	
	public void loadExplosionGif() {
//		Position position = this.position;
		loadImage("images/explosion.gif");
	}
	
	public void loadBigExplosionGif(Spaceship spaceship) {
		loadImage("images/explosion.gif");
		width = spaceship.getWidth();
		height = spaceship.getHeight();
	}

    public void explode(Position position, ArrayList<Explosion> explosionList) {
    	Explosion explosion = new Map().spawnExplosion(position);
    	Thread newThread = new Thread(new Runnable() {
    		public void run() {
    			try{
    				Thread.sleep(1000);
    				explosionList.remove(explosion);
    			} catch(Exception e) {
    				
    			}
    		}
    		
    	});
    	newThread.start();
    }
}

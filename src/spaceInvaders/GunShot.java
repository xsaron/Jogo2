package spaceInvaders;

//import java.awt.Graphics;
//import java.awt.event.KeyEvent;
//import java.awt.image.ImageObserver;


public class GunShot extends Sprite {
	
	private static final int SHOT_SPEED = -4;
	
	public GunShot(Position position) {
		super(position);
//		this.shoot(position);
	}
	
	public void shoot(Position position) { 
		loadImage("images/missile1.png");
		position.setY(position.getY() + SHOT_SPEED);
	}
}

package spaceInvaders;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;
import java.util.Timer;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
//import javax.swing.Timer;

import spaceInvaders.RankingMenu.EnterName;

public class Map extends JPanel implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private final int EASY = 1;
    private final int MEDIUM = 2;
    private final int HARD = 3;
    private final int SCORE = 1050;
    public boolean gameOver = false;
    private final javax.swing.Timer timer_map;
    private final javax.swing.Timer timer_enemy;
    private final javax.swing.Timer timer_shield;
    private final int SHOT_RADIUS = 10;
    private final int INITIAL_SPAWN_POSITION_Y = -10;
    private int bonusCounter = 10;
    private float energyPercentage;
    private float shieldPercentage;
    
    private final Image background;
    private final Spaceship spaceship;
//    private java.util.Timer timer = new Timer();

    private final ArrayList<Enemy> enemyList = new ArrayList();
    private final ArrayList<Enemy> mediumEnemyList = new ArrayList();
    private final ArrayList<Enemy> hardEnemyList = new ArrayList();
    private final ArrayList<Bonus> bonusList = new ArrayList();
    private final ArrayList<Explosion> explosionList = new ArrayList();

    public Map() {
        
        addKeyListener((KeyListener) new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();
        
        final Position initialSpaceshipPosition = new Position(SPACESHIP_X, SPACESHIP_Y);
        spaceship = new Spaceship(initialSpaceshipPosition);

        timer_map = new javax.swing.Timer(Game.getDelay(), this);
        timer_map.start();
        
        ActionListener spawnEnemy = new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		spawnEnemy(enemyList);
        		if(enemyList.size() >= 10) {
            		spawnEnemy(mediumEnemyList);
            		bonusCounter++;
            		bonusCounter++;
            		spawnBonus();
        		}
        		
        		if(mediumEnemyList.size() >= 15) {
        			spawnEnemy(hardEnemyList);
//        			bonusCounter--;
        		}
        	}
        };
        
        timer_enemy = new javax.swing.Timer(700, spawnEnemy);  
        timer_enemy.start();
        
        ActionListener rechargeShield = new ActionListener() {
        	public void actionPerformed(ActionEvent evt) {
        		spaceship.rechargeShield();
        	}
        };
        
        timer_shield = new javax.swing.Timer(7000, rechargeShield);
        timer_shield.start();
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
       
        draw(g);

        Toolkit.getDefaultToolkit().sync();
    }

    private void draw(Graphics g) {
               
       
        // Draw shot
        final int shipSymetry = spaceship.getWidth()/2 - this.SHOT_RADIUS/2;
        for(GunShot gunShot : spaceship.getAmmo()){
        	g.drawImage(gunShot.getImage(), gunShot.getPosition().getX() + shipSymetry, gunShot.getPosition().getY() + 2*this.SHOT_RADIUS, this.SHOT_RADIUS, this.SHOT_RADIUS, this);
    	}
        
        // Draw spaceship
        g.drawImage(spaceship.getImage(), spaceship.position.getX(), spaceship.position.getY(), this);
        spaceship.updateEnergyByMoving();
        
        drawSpaceshipStatus(g);
        
        // Draw Score
        g.drawString("Score: " + spaceship.getScore(), 25, 25);
        
        
        // Draw enemies
        for(Enemy enemy : enemyList){
        	g.drawImage(enemy.getImage(), enemy.getPosition().getX(), enemy.getPosition().getY(), this);
        }
        
        for(Enemy enemy : mediumEnemyList){
        	g.drawImage(enemy.getImage(), enemy.getPosition().getX(), enemy.getPosition().getY(), this);
        }
        
        for(Enemy enemy : hardEnemyList){
        	g.drawImage(enemy.getImage(), enemy.getPosition().getX(), enemy.getPosition().getY(), this);
        }
        
        // Draw bonus
        for(Bonus bonus : bonusList) {
        	g.drawImage(bonus.getImage(), bonus.getPosition().getX(), bonus.getPosition().getY(), this);
        }
        
        // Draw explosions
        Iterator<Explosion> itrExplosion = explosionList.iterator();
        while(itrExplosion.hasNext()) {
        	Explosion explosion = itrExplosion.next();
        	g.drawImage(explosion.getImage(), explosion.getPosition().getX(), explosion.getPosition().getY(), this);

        }
        
        if(spaceship.getEnergy() == 0) {
//        	spaceship.noEnergy();
        }
        
        if(spaceship.getShield() <= 0 && gameOver == false) {
        	gameOver = true;
            endGame();
        	
        }
        
        if(gameOver) {
            drawGameOver(g);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	
    	testAllCollision();
        updateSpaceship();
        updateGunShot();
        updateAllEnemies();
        updateBonus();
       
        repaint();
    } 
    
    private void updateSpaceship() {
        spaceship.move();
    }
    
    private void updateGunShot() {
    	
    	ArrayList<GunShot> ammoList = spaceship.getAmmo();
    	
    	if(!ammoList.isEmpty()) {
    		for(GunShot gunShot : ammoList) {
    			gunShot.shoot(gunShot.position);
    		}
    	}
    }
    
    private void updateEnemies(ArrayList<Enemy> enemyList, int dificult) {
    	ArrayList<Enemy> _enemyList = enemyList;
    	
    	if(!_enemyList.isEmpty()) {
    		Iterator<Enemy> itr = enemyList.iterator();
    		while(itr.hasNext()) {
    			Enemy enemy = itr.next();
    			enemy.fly(dificult);
    		}
    	}
    	
    	
    }
    
    private void updateAllEnemies() {
    	updateEnemies(enemyList, new Enemy().EASY);
        updateEnemies(mediumEnemyList, new Enemy().MEDIUM);
        updateEnemies(hardEnemyList, new Enemy().HARD);
    }
    
    private void updateBonus() {
    	for(Bonus bonus : bonusList) {
    		bonus.move();
    	}
    }
    
    private Enemy spawnEnemy(ArrayList<Enemy> enemyList) {
    		Position randomPosition = new Position(new Random().nextInt(Game.getWidth()), INITIAL_SPAWN_POSITION_Y);
    		Enemy enemy = new Enemy(randomPosition);
    		
    		enemyList.add(enemy);
    		
    		return enemy;
    		
    }
    
    private void spawnBonus() {
    	if(bonusCounter % 10 == 0) {
        	Position randomPosition = new Position(new Random().nextInt(Game.getWidth()), INITIAL_SPAWN_POSITION_Y);
        	Bonus bonus = new Bonus(randomPosition);
        	
        	bonusList.add(bonus);
        	bonusCounter = 10;
    	}
    }
    
    public Explosion spawnExplosion(Position position) {
    	Explosion explosion = new Explosion(position);
    	
    	explosionList.add(explosion);
    	return explosion;
    }
    
    private void explode(Position position) {
    	Explosion explosion = spawnExplosion(position);
    	Thread newThread = new Thread(new Runnable() {
    		public void run() {
    			try{
    				Thread.sleep(1000);
    				explosionList.remove(explosion);
    			} catch(Exception e) {
    				
    			}
    		}
    		
    	});
    	newThread.start();
    }
    
    
    private void testEnemyCollision(Spaceship spaceship, ArrayList<Enemy> enemyList, int dificult) {
    	Iterator<Enemy> itr = enemyList.iterator();
    	while(itr.hasNext()) {
    		Enemy enemy = itr.next();
    		Rectangle enemyCollisionBox = new Rectangle(enemy.getBounds());
			Rectangle playerCollisionBox = new Rectangle(spaceship.getBounds());
			
			if(enemyCollisionBox.intersects(playerCollisionBox)) {
				explode(enemy.getPosition());
				spaceship.takeDamage(dificult);
				itr.remove();
				
			}
    	}
    	
    	Iterator<GunShot> itrAmmo = spaceship.getAmmo().iterator();
    	while(itrAmmo.hasNext()) {
    		GunShot gunShot = itrAmmo.next();
    		Rectangle missileCollisionBox = new Rectangle(gunShot.getBounds());
    		
    		Iterator<Enemy> itrEnemy = enemyList.iterator();
    		while(itrEnemy.hasNext()) {
    			Enemy enemy = itrEnemy.next();
        		Rectangle enemyCollisionBox = new Rectangle(enemy.getBounds());
        		
        		if(enemyCollisionBox.intersects(missileCollisionBox)) {
        			explode(enemy.getPosition());
        			itrEnemy.remove();
        			itrAmmo.remove();
        			spaceship.setScore(spaceship.getScore() + this.SCORE);
        		}
    		}	
    	}
    }
    
    private void testBonusCollision() {
    	Iterator<Bonus> itr = this.bonusList.iterator();
    	while(itr.hasNext()) {
    		Bonus bonus = itr.next();
    		Rectangle bonusCollisionBox = new Rectangle(bonus.getBounds());
    		Rectangle spaceshipCollisionBox = new Rectangle(this.spaceship.getBounds());
    		
    		if(bonusCollisionBox.intersects(spaceshipCollisionBox)) {
    			itr.remove();
    			if((spaceship.getEnergy() + spaceship.initialEnergy/100*12) > spaceship.initialEnergy) {
    				spaceship.setEnergy(spaceship.initialEnergy);
    			} else {
        			spaceship.setEnergy(spaceship.getEnergy() + spaceship.initialEnergy/100*12);

    			}
    		}
    	}    
    }
    
    private void testAllCollision() {
    	testEnemyCollision(spaceship, enemyList, EASY);
    	testEnemyCollision(spaceship, mediumEnemyList, MEDIUM);
    	testEnemyCollision(spaceship, hardEnemyList, HARD);
    	testBonusCollision();
    }
   
    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }
        
        
    }
    
    private void endGame() {
		if(gameOver) {

	    	Thread newThread = new Thread(new Runnable() {
	    		public void run() {
	    			try{
	        			Thread.sleep(3000);
	        			RankingMenu ranking = new RankingMenu();
	        			ranking.new EnterName();
	        			ranking.setScore(spaceship.getScore());
	        			setVisible(false);
	    			} catch (Exception e){
	    				System.out.println("gera exceção");
	    			}
	    			
	    		}
	    	});
	    	newThread.start();
	    }
    }
       
    private void drawGameOver(Graphics g) {

        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 24);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void drawSpaceshipStatus(Graphics g) {
    	
    	
    	energyPercentage = (spaceship.getEnergy()/spaceship.initialEnergy)*100;
        float ePercentage = Math.round(energyPercentage);
        String ePercent = "Energy" + ePercentage + "%";
        Font font = new Font("Helvetica", Font.BOLD, 16);
        FontMetrics metric = getFontMetrics(font);
        g.setColor(Color.gray);
        g.setFont(font);
        
        g.drawString("ϟ " + ePercentage + "%", spaceship.getPosition().getX(), spaceship.getPosition().getY() + spaceship.getHeight()*3/2);
 
        shieldPercentage = (spaceship.getShield()/spaceship.initialShield)*100;
        float sPercentage = Math.round(shieldPercentage);
        g.drawString("° " + sPercentage + "%", spaceship.getPosition().getX(), spaceship.getPosition().getY() + spaceship.getHeight()*7/4);
        
    }
}

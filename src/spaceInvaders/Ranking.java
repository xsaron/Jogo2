package spaceInvaders;

import java.io.Serializable;


public class Ranking implements Serializable, Comparable {
	String name;
	double score;
	
	public Ranking(String name, double score) {
		this.score = score;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public double getScore() {
		return score;
	}
	
	@Override
	public String toString() {
		String s = String.format("%d", (int) score);
		return name + " " + s;
		
	}

	@Override
	public int compareTo(Object arg0) {
		if(arg0 instanceof Ranking) {
			Double a = new Double(score);
			Double b = new Double(((Ranking)arg0).getScore());
			
			return -(a.compareTo(b));
		}
		return 0;
	}
	
	
}

package spaceInvaders;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

public class RankingList extends JPanel{
	
	public RankingList() {
		
		ArrayList<Ranking> rang = RankingMaker.readRanking();
		Ranking[] r = (Ranking[]) rang.toArray(new Ranking[rang.size()]);

		DefaultListModel model = new DefaultListModel();
		
		for (Ranking r1 : rang) {
			model.addElement(r1.toString());
		}
		
		JList list = new JList();
		list.setModel(model);
		add(list);
		setPreferredSize(new Dimension(Game.getWidth()/2, Game.getHeight()/2));
		list.setPreferredSize(new Dimension(Game.getWidth()/2, Game.getHeight()/2));
		list.setAlignmentY(CENTER_ALIGNMENT);
		list.setVisible(true);
		setVisible(true);
	}
}

package spaceInvaders;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import spaceInvaders.Ranking;

public class RankingMaker {
	private static Path path = Paths.get("./Ranking.txt");
	public static void writeRanking(Ranking ranking) {
		ArrayList<Ranking> rankingList = readRanking();
		rankingList.add(ranking);
		Collections.sort(rankingList);
		FileOutputStream fout = null;
		ObjectOutputStream oos = null;
		
		try {

			fout = new FileOutputStream(path.toString());
			oos = new ObjectOutputStream(fout);
			oos.writeObject(rankingList);

			System.out.println("Done");

		} catch (Exception ex) {

			ex.printStackTrace();

		} finally {

			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static ArrayList<Ranking> readRanking() {
		
		ArrayList<Ranking> ranking = null;

		FileInputStream fin = null;
		ObjectInputStream ois = null;

		try {

			fin = new FileInputStream(path.toString());
			ois = new ObjectInputStream(fin);
			Object object = ois.readObject();
			if(object instanceof ArrayList) {
				ranking = (ArrayList<Ranking>) object;
			} else {
				ranking = new ArrayList<Ranking>();
			}
			

		} catch (Exception ex) {
			ranking = new ArrayList<Ranking>();
			
		} finally {

			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		for(Ranking r : ranking) {
			System.out.println(r.getName() + r.getScore());

		}

		return ranking;
	}
}

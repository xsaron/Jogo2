package spaceInvaders;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import spaceInvaders.Application.Menu;

public class RankingMenu extends JFrame{
	private double score;
	
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public RankingMenu() {
		
		setSize(Game.getWidth(), Game.getHeight());
		setTitle("Space Through Game - Ranking");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setLocationRelativeTo(null);
	    
	    JButton backButton = new JButton("Voltar");
	    
	    Dimension buttonDimension = new Dimension(120, 60);
	    Dimension panelDimension = new Dimension(Game.getWidth(), 60);
	    
	    backButton.setPreferredSize(buttonDimension);
	    
	    Event f = new Event();
	    backButton.addActionListener(f);
	    
	    JPanel buttonPanel = new JPanel();
	    JPanel panelPanel = new JPanel();
	    panelPanel.add(buttonPanel);
	    panelPanel.add(new RankingList());
	    buttonPanel.add(backButton);
	    buttonPanel.setPreferredSize(panelDimension);
	    add(panelPanel);	    
	   
	}
	
	public class Event implements ActionListener {
	
		@Override
		public void actionPerformed(ActionEvent f) {
//			Menu menu = new Application() .new Menu();
//			menu.setVisible(true);
			setVisible(false);
			
		}
	}
	
	public class EnterName extends JFrame {
		
		public EnterName() {
			setSize(Game.getWidth(), Game.getHeight());
			setTitle("Space Through Game - Ranking");
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    setLocationRelativeTo(null);
		    
		    JButton enter = new JButton("Confirmar");
		    
		    
		    Dimension buttonDimension = new Dimension(120, 60);
		    Dimension panelDimension = new Dimension(Game.getWidth(), 60);
		    Dimension textFieldDimension = new Dimension(200, 30);

		    enter.setPreferredSize(buttonDimension);
		    
		    JTextField tField = new JTextField();
		    tField.setPreferredSize(textFieldDimension);
		    
		    JPanel textPanel = new JPanel();
		    textPanel.setPreferredSize(panelDimension);
		    
		    textPanel.add(tField);
		    textPanel.add(enter);
		    add(textPanel);
		    
		    enter.addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent evt) {
		    		Ranking ranking = new Ranking(tField.getText(), getScore());
		    		RankingMaker.writeRanking(ranking);
		    		setVisible(false);
		    		new Application().new Menu().setVisible(true);
		    	}
		    });
		    
		    setVisible(true);
		
		    
		}
		
	}
}


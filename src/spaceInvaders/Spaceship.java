package spaceInvaders;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Spaceship extends Sprite {
    
    private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 2;
    private static final int DAMAGE = 500;
    public final float initialShield = 10000;
    private final int initialScore = 0;
    public final float initialEnergy = 100000;
    private float shieldPercentage;
    
   
    private int speed_x;
    private int speed_y;
    private float energy = initialEnergy;
    private float shield = initialShield;
    private int score = initialScore;
    
    public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public float getEnergy() {
		return energy;
	}

	public void setEnergy(float energy) {
		this.energy = energy;
	}
	
	public float getShield() {
		return shield;
	}
	
	public void setShield(float shield) {
		this.shield = shield;
	}
	
	public void takeDamage(int dificult) {
		if(getShield() - DAMAGE * dificult > 0) {
			setShield(getShield() - DAMAGE * dificult);
		} else {
			setShield(0);
		}
		
		shieldPercentage = getShield()/initialShield * 100;
	}
	
	public void updateEnergyByMoving() {
		if(getEnergy() - initialEnergy/10000 > 0) {
			setEnergy(getEnergy() - initialEnergy/10000);
		} else {
			setEnergy(0);
		}
		
	}
	
	public void rechargeShield() {
		if(getShield() != initialShield && getShield() != 0) {
			setShield(getShield() + initialShield/100);
		} else if(getShield() == initialShield){
			setShield(initialShield);
		} else if(getShield() == 0) {
			setShield(0);
		}
	}

	private ArrayList<GunShot> ammo = new ArrayList<GunShot>();

    public Spaceship(Position position) {
        super(position);
        
        initSpaceShip();
    }

    private void initSpaceShip() {
        
        thrust();
        
    }
    
    private void noThrust(){
        loadImage("images/spaceship.gif"); 
    }
    
    private void thrust(){
        loadImage("images/spaceship.gif"); 
    }    
    
    private void thrustLeft() {
    	loadImage("images/spaceship_left.gif");
    }
    
    private void thrustRight() {
    	loadImage("images/spaceship_right.gif");
    }
    
    public void noEnergy() {
    	loadImage("images/spaceship_dead.gif");
//    	setX();
    }
    
    private void explode() {
    	new Explosion(new Position(0,0)).loadBigExplosionGif(this);
    }
    
    public void shoot() {
    	
    	if(getEnergy() > 0 || getShield() <= 0) {
    		GunShot newGunShot = new GunShot(this.position);
        	this.ammo.add(newGunShot);
        	ArrayList<GunShot> ammo = this.ammo;
        	Thread newThread = new Thread(new Runnable() {
        		public void run() {
        			try{
        				Thread.sleep(1500);
        				ammo.remove(newGunShot);
        			} catch(Exception e) {
        				
        			}
        		}
        		
        	});
        	newThread.start();
        	
        	setEnergy(getEnergy() - initialEnergy/100);
    	}
    	
    	
    }
    
    public ArrayList<GunShot> getAmmo() {
		return ammo;
	}

	public void move() {
        
		if(getEnergy() > 0) {
			// Limits the movement of the spaceship to the side edges.
	        if((speed_x < 0 && super.position.getX() <= 0) || (speed_x > 0 && super.position.getX() + width >= Game.getWidth())){
	            speed_x = 0;
	        }
	        
	        // Moves the spaceship on the horizontal axis
	        final Position newPositionX = new Position(super.position.getX() + speed_x, super.position.getY());
	        super.setPosition(newPositionX);
	        
	        // Limits the movement of the spaceship to the vertical edges.
	        if((speed_y < 0 && super.position.getY() <= 0) || (speed_y > 0 && super.position.getY() + height >= Game.getHeight())){
	            speed_y = 0;
	        }

	        // Moves the spaceship on the vertical axis
	        final Position newPositionY = new Position(super.position.getX(), super.position.getY() + speed_y);
	        super.setPosition(newPositionY);
		}
        
        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        if(getEnergy() > 0 && getShield() > 0) {
        	if (key == KeyEvent.VK_LEFT) {
                speed_x = -1 * MAX_SPEED_X;
                thrustLeft();
            
            // Set speed to move right
            } else if (key == KeyEvent.VK_RIGHT) {
                speed_x = MAX_SPEED_X;
                thrustRight();
                
            // Set speed to move up
            } else if (key == KeyEvent.VK_UP) {
                speed_y = -1 * MAX_SPEED_Y;
//                thrustPlus();
                thrust();
                
            // Set speed to move down
            } else if (key == KeyEvent.VK_DOWN) {
                speed_y = MAX_SPEED_Y;
                noThrust();
            
            // Shot
            } else if (key == KeyEvent.VK_SPACE) {
//            	position.setY(position.getY() + 4);
            	shoot();
            }
//        } else if(getEnergy() <= 0) {
//        	noEnergy();
//        } else {
//        	explode();
        }
        
        
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();
        if(getEnergy() > 0 && getShield() > 0) {
        	if (key == KeyEvent.VK_UP) {
            	speed_y = 0;
            	thrust();
            }
            
            if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
                speed_x = 0;
                thrust();
            }

            if (/*key == KeyEvent.VK_UP ||*/ key == KeyEvent.VK_DOWN) {
                speed_y = 0;
                thrust();
            }
//        } else if(getShield() == 0){
//        	explode();
//        } else {
//        	noEnergy();
        }
        
    }
}

